## Arcricket

A digital scoreboard for weekly games of Cricket (darts) after work. (Arcurve + cricket = Arcricket)

*This project was bootstrapped with Create React App*
___

[Click here](https://www.figma.com/file/ZvwUKffReE79S5SXwsIZRtg4/Mockups?node-id=0%3A1) to see a mockup of the design of the app

---

The typical react script can be used to build and run the application:
```
npm install
npm start
```
---
To build and deploy the application:
```
npm run build
firebase deploy
```
You will need to install the [Firebase CLI](https://github.com/firebase/firebase-tools) and have the correct credentials to deploy


---
To preview/manually test UI components, you can use Storybook

```
npm run storybook
```

https://storybook.js.org/docs/basics/introduction/

You can change props with "Knobs" and see callbacks with "Actions" -- through
the storybook UI

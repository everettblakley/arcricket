import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, object } from '@storybook/addon-knobs';
import {BrowserRouter as Router, Route, withRouter} from "react-router-dom";

import '../../App.scss';
import '../../index.css';

const ShowTheLocation = ({ location }) => (
    <div style={{marginTop: 100}}>You are now at {location.pathname}</div>
)

const ShowTheLocationWithRouter = withRouter(ShowTheLocation);

import Navbar from './Navbar';
storiesOf('Navbar', module)
  .addDecorator(withKnobs)
  .addDecorator(storyFn => (
    <Router>
      {storyFn()}
      <ShowTheLocationWithRouter />
    </Router>
  ))
  .add('tablet', () => <Navbar />, { viewport: { defaultViewport: 'ipad' } })
  .add('mobile', () => <Navbar />, { viewport: { defaultViewport: 'iphonex' } });

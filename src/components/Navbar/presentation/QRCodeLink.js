import React from 'react';
import QRCode from 'qrcode.react';

const QRCodeLink = () => (
  <QRCode value={window.location.href} bgColor="#A0272B" includeMargin={true} renderAs="svg" fgColor="#e9e9e9" size={200} />
);

export default QRCodeLink;

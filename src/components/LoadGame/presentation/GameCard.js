import React from "react";
import moment from "moment";

const GameCard = ({gameData, isActive, endGame, loadGame}) => (
    <div className="game-card" key={`game-${gameData.id}`}>
        <div className="game-card-body">
            <div className="card-teams">
                {gameData.teams.map((team, index) =>
                    <p key={`team-${index}`}><b>{`Team ${index + 1}: `}</b>{team.players.join(", ")}</p>
                )}
                <p className="text-muted time">
                    {`Created: ${moment(gameData.startDate.toDate()).format("LLLL")}`}
                </p>
                {!isActive && <p className="text-muted time">
                    {`Ended: ${moment(gameData.endDate.toDate()).format("LLLL")}`}
                </p>}
            </div>
            <div className="card-buttons">
                <button className="btn card-button dark-red" onClick={() => loadGame(gameData.id)}>Load Game</button>
                {isActive &&
                <button className="btn card-button light-grey" onClick={() => endGame(gameData.id)}>End
                    Game</button>}
            </div>
        </div>
    </div>
);

export default GameCard;
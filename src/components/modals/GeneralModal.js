import React from "react";
import Modal from "react-modal";
import PropTypes from "prop-types";

const modalStyles = {
	content: {
		top: "50%",
		left: "50%",
		right: "auto",
		bottom: "auto",
		marginRight: "-50%",
		transform: "translate(-50%, -50%)"
	}
};

const GeneralModal = ({title, isOpen, text}) => {
	return (
		<Modal isOpen={isOpen} style={modalStyles} shouldCloseOnOverlayClick={true} shouldCloseOnEsc={true}>
			<div style={{padding: "50px"}}>
				<h4>{title}</h4>
				<p>{text}</p>
			</div>
		</Modal>
	);
};

GeneralModal.propTypes = {
	title: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired,
	isOpen: PropTypes.bool.isRequired,
};

export default GeneralModal;

import React, {Component} from "react";
import Modal from "react-modal";
import PropTypes from "prop-types";

const modalStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)"
    }
};

const options = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

class ScoreModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hit: 0
        };
    }

    render() {
        const multiplier = this.props.multiplier === "D" ? 2 : this.props.multiplier === "T" ? 3 : 0;
        const multiplierString = this.props.multiplier === "D" ? "double" : this.props.multiplier === "T" ? "triple" : "";
        const {hit} = this.state;
        return (
            <Modal isOpen={this.props.isOpen} style={modalStyles}>
                <div style={{padding: "25px"}}>
                    <h3>Nice! You scored a {multiplierString}!</h3>
                    <h5>What'd you hit?</h5>
                    <div style={{width: 500, display: "flex", flexDirection: "row", flexWrap: "wrap", margin: "15px 0"}}>
                        {options.map((option) =>
                            <button
                                className={`btn ${this.state.hit === option ? "dark-red" : "dark-grey"} multiplier-button`}
                                onClick={() => this.setState({hit: option})}
                                key={option}>{option}</button>)}
                    </div>
                    {hit === 0 ? `Select a number` : `Add ${this.state.hit} * ${multiplier} = ${this.state.hit * multiplier} to your score?`}
                    <div className="submit-modal-buttons" style={{alignSelf: "flex-end"}}>
                        <button className="btn dark-red" disabled={hit === 0} onClick={() => this.props.addByMultiplier(this.state.hit)}>Yup!
                        </button>
                        <button className="btn dark-grey" onClick={this.props.onCancel}>Cancel
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

ScoreModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    multiplier: PropTypes.string,
    addByMultiplier: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
};

export default ScoreModal;

import React from "react";
import Modal from "react-modal";
import PropTypes from "prop-types";

const modalStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)"
    }
};

const SubmitModal = ({teams, isOpen, onSubmit, onCancel}) => {
    return (
        <Modal isOpen={isOpen} style={modalStyles}>
            <div style={{padding: "50px"}}>
                <h4>Ready to submit this game?</h4>
                {teams.map((team, index) => (
                    <div key={"team-" + index}>
                        <b>Team {index + 1}: </b>
                        {team.join(", ")}
                    </div>
                ))}
                <div className="submit-modal-buttons">
                    <button className="btn dark-red" onClick={onSubmit}>Submit
                    </button>
                    <button className="btn dark-grey" onClick={onCancel}>Cancel
                    </button>
                </div>
            </div>
        </Modal>
    );
};

SubmitModal.propTypes = {
    teams: PropTypes.array.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
};

export default SubmitModal;

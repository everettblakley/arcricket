import React from "react";
import Modal from "react-modal";
import PropTypes from "prop-types"

const modalStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        padding: "50px"
    }
};

const LoadingModal = ({isVisible, text}) => {
    return (
        <Modal isOpen={isVisible} shouldCloseOnOverlayClick={false} shouldCloseOnEsc={false}  style={modalStyles}>
            <div>
                <h1>{text}</h1>
                <img src={require("../../images/dartboard.svg")} className="loading-dartboard" height={100} width={100} alt="loading"/>
            </div>
        </Modal>
    );
};

LoadingModal.propTypes = {
    isVisible: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
};

export default LoadingModal;
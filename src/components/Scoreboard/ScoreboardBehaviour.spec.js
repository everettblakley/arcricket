import ScoreboardBehaviour from './ScoreboardBehaviour';

describe('ScoreboardBehaviour', () => {
  let behaviour;
  let teams;

  beforeEach(() => {
    behaviour = new ScoreboardBehaviour();
    teams = [
      { scores: { openGroup: 2, closedGroup: 3 } },
      { scores: { openGroup: 3, closedGroup: 3 } },
      { scores: { openGroup: 3, closedGroup: 3 } },
    ];
  });
  
  describe('addByMultiplier', () => {
    it('returns twice the score if the multiplier is "D"', () => {
      expect(behaviour.addByMultiplier(4, 'D')).toEqual(8);
    });
    
    it('returns three times the score if the multiplier is "T"', () => {
      expect(behaviour.addByMultiplier(4, 'T')).toEqual(12);
    });
    
    it('returns zero if the score multiplier is neither "T" nor "D"', () => {
      expect(behaviour.addByMultiplier(4, 'E')).toEqual(0);
    });
  });

  describe('groupIsClosed', () => {
    it('returns false if the key is "Points"', () => {
      expect(behaviour.groupIsClosed(teams, 'Points')).toEqual(false);
    });

    it('returns false if any team has a score less than three for the given key', () => {
      expect(behaviour.groupIsClosed(teams, 'openGroup')).toEqual(false);
    });

    it('returns true if all teams have a score of three for the given key', () => {
      expect(behaviour.groupIsClosed(teams, 'closedGroup')).toEqual(true);
    });
  });

  describe('pointsAvailable', () => {
    it('returns false if the key is "Points"', () => {
      expect(behaviour.pointsAvailable(teams, 'Points', 0)).toEqual(false);
    });

    it('returns false if all teams have a score of three for the given key', () => {
      expect(behaviour.pointsAvailable(teams, 'closedGroup', 0)).toEqual(false);

    });

    it('returns true if any team has a score of less than three for the given key, but the current team has a score of three', () => {
      expect(behaviour.pointsAvailable(teams, 'openGroup', 1)).toEqual(true);
    });

    it('returns false if any team has a score of less than three for the given key, and the current team has a score less than three', () => {
      expect(behaviour.pointsAvailable(teams, 'openGroup', 0)).toEqual(false);
    });
  });

  describe('handleScoreIncrease', () => {
    //handleScoreIncrease(teams, teamIndex, scoreKey) {
    it('returns an empty object if the group is closed', () => {
      expect(behaviour.handleScoreIncrease(teams, 0, 'closedGroup')).toEqual({});
    });

    it('returns openTheModal if the group is not closed and the current team can get points and the key is "T"', () => {
      teams = [
        { scores: { T: 1 } },
        { scores: { T: 3 } },
      ];
      expect(behaviour.handleScoreIncrease(teams, 1, 'T')).toEqual({ openTheModal: true });
    });

    it('returns openTheModal if the group is not closed and the current team can get points and the key it "D"', () => {
      teams = [
        { scores: { D: 1 } },
        { scores: { D: 3 } },
      ];
      expect(behaviour.handleScoreIncrease(teams, 1, 'D')).toEqual({ openTheModal: true });
    });

    it('returns updateTeams with the score for the current team & key increased by 1 when the team cannot get points can the key is "T"', () => {
      teams = [
        { scores: { T: 1 } },
        { scores: { T: 3 } },
      ];
      expect(behaviour.handleScoreIncrease(teams, 0, 'T')).toEqual({ updateTeams: [
        { scores: { T: 2 } },
        { scores: { T: 3 } },
      ]});
    });

    it('returns updateTeams with the score for the current team & key increased by 1 when the team cannot get points can the key is "D"', () => {
      teams = [
        { scores: { D: 1 } },
        { scores: { D: 3 } },
      ];
      expect(behaviour.handleScoreIncrease(teams, 0, 'D')).toEqual({ updateTeams: [
        { scores: { D: 2 } },
        { scores: { D: 3 } },
      ]});
    });

    [
      ["15", 34],
      ["16", 35],
      ["17", 36],
      ["18", 37],
      ["19", 38],
      ["20", 39],
      ["B", 44],
    ].forEach(([ scoreKey, newPoints ]) => {
      it(`returns updateTeams with the points for the current team increased from 19 to ${newPoints} when the key is ${scoreKey} and the team cannot score points`, () => {
        teams = [
          { scores: { [scoreKey]: 1, points: 19 } },
          { scores: { [scoreKey]: 3, points: 19 } },
        ];
        expect(behaviour.handleScoreIncrease(teams, 1, scoreKey)).toEqual({ updateTeams: [
          { scores: { [scoreKey]: 1, points: 19 } },
          { scores: { [scoreKey]: 3, points: newPoints } },
        ]});
      });

      it(`returns updateTeams with the score incremented when the key is ${scoreKey} and the team cannot score points`, () => {
        teams = [
          { scores: { [scoreKey]: 1, points: 19 } },
          { scores: { [scoreKey]: 3, points: 19 } },
        ];
        expect(behaviour.handleScoreIncrease(teams, 0, scoreKey)).toEqual({ updateTeams: [
          { scores: { [scoreKey]: 2, points: 19 } },
          { scores: { [scoreKey]: 3, points: 19 } },
        ]});
      });
    });
  });
});

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, object } from '@storybook/addon-knobs';

import '../../../App.scss';
import '../../../index.css';

import ScoreboardPanels, { scores } from './ScoreboardPanels';

function randomScores(start) {
  const result = {};
  const max = 3;
  const min = 0;
  scores.filter(s => s !== 'Points').forEach(key => {
    result[key] = Math.floor(Math.random() * (max - min + 1)) + min;
  });
  return { ...result, ...start };
}

const teams = [
  { players: ['The First'], scores: randomScores({ points: 0 }) },
  { players: ['The Greatest'], scores: randomScores({ points: 49 }) },
  { players: ['The Penultimate'], scores: randomScores({ points: 2 }) },
]

storiesOf('ScoreboardPanels', module)
  .addDecorator(withKnobs)
  .add('basic', () => <ScoreboardPanels
      teams={object('teams', teams)}
      groupIsClosed={key => false}
      pointsAvailable={key => key !== 'Points'}
      onSubmitTeam={action('onSubmitTeam')}
      onUpdateTeam={action('onUpdateTeam')}
      onSubmitPoints={action('onSubmitPoints')}
      onUpdatePoints={action('onUpdatePoints')}
      decreaseScore={action('decreaseScore')}
      increaseScore={action('increaseScore')}
      addTeam={action('addTeam')}
  />);

# Presentation Components

The idea behind these presentation components is that they are stateless and
focus on UI/presentation. The pattern has also been called smart/dumb
components. Read more: https://alligator.io/react/smart-dumb-components/

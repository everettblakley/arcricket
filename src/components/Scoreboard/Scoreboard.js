import React, {Component} from "react";
import firebase from "../../config/firebase-config";
import LoadingModal from "../modals/LoadingModal";
import ScoreModal from "../modals/ScoreModal";
import ScoreboardPanels from "./presentation/ScoreboardPanels";
import ScoreboardBehaviour from "./ScoreboardBehaviour";

const db = firebase.firestore();

class Scoreboard extends Component {
    constructor(props) {
        super(props);
        this.behaviour = new ScoreboardBehaviour();

        this.state = {
            game: null,
            loadingModalOpen: false,
            scoreModalOpen: false,
            selectedTeamScoreMultiplier: undefined,
            selectedTeamIndex: 0,
        };
    }

    addByMultiplier = (hit) => {
        const {game, selectedTeamScoreMultiplier, selectedTeamIndex} = this.state;
        game.teams[selectedTeamIndex].scores.points += this.behaviour.addByMultiplier(hit, selectedTeamScoreMultiplier)
        this.setState({scoreModalOpen: false, loadingModalOpen: true}, () => {
            db.collection("games")
                .doc(this.props.match.params.id)
                .update({teams: game.teams})
                .then(() => this.setState({
                    loadingModalOpen: false,
                    selectedTeamScoreMultiplier: null,
                    selectedTeamIndex: 0
                }))
                .catch((error) => {
                    console.log(error);
                    this.setState({
                        loadingModalOpen: false,
                        selectedTeamScoreMultiplier: null,
                        selectedTeamIndex: 0,
                    });
                });
        });
    };

    openScoreModal(teamIndex, scoreKey) {
        this.setState({
            selectedTeamIndex: teamIndex,
            selectedTeamScoreMultiplier: scoreKey,
            scoreModalOpen: true
        });
    }

    increaseScore = (teamIndex, scoreKey) => {
        const {game} = this.state;
        const { openTheModal, updateTeams } = this.behaviour.handleScoreIncrease(game.teams, teamIndex, scoreKey);
        if(openTheModal) {
            this.openScoreModal(teamIndex, scoreKey);
        } else if(updateTeams) {
            game.teams = updateTeams;
            this.updateGame(game);
        }
    };

    decreaseScore = (teamIndex, scoreKey) => {
        const {game} = this.state;
        game.teams[teamIndex].scores[`${scoreKey}`] = game.teams[teamIndex].scores[`${scoreKey}`] > 0 ? (game.teams[teamIndex].scores[`${scoreKey}`] - 1) % 4 : 0;
        this.updateGame(game);
    };

    updateGame = (game) => {
        this.setState({loadingModalOpen: true}, () => {
            db.collection("games")
                .doc(this.props.match.params.id)
                .update({teams: game.teams})
                .then(() => this.setState({loadingModalOpen: false}))
                .catch((error) => {
                    console.log(error);
                    this.setState({loadingModalOpen: false});
                });
        });
    };

    onUpdatePoints = (e) => {
        e.preventDefault();
        const {game} = this.state;
        console.log((e.target.value));
        game.teams[e.target.id].scores.points = e.target.value;
        this.setState({game});
    };

    onSubmitPoints = (e) => {
        e.preventDefault();
        this.updateGame(this.state.game);
    };

    onUpdateTeam = (e) => {
        e.preventDefault();
        const {game} = this.state;
        game.teams[e.target.id].players = e.target.value.split(", ");
        this.setState({game});
    };

    onSubmitTeam = (e) => {
        e.preventDefault();
        this.updateGame(this.state.game);
    };

    addTeam = (e) => {
        e.preventDefault();
        const {game} = this.state;
        game.teams.push({
            id: game.teams.length,
            players: ["New player"],
            scores: {
                "20": 0,
                "19": 0,
                "18": 0,
                "17": 0,
                "16": 0,
                "15": 0,
                "T": 0,
                "D": 0,
                "B": 0,
                "points": 0
            }
        });
        this.updateGame(game);
    };


    onScoreModalCancel = () => this.setState({
        scoreModalOpen: false,
        selectedTeamScoreMultiplier: undefined,
        selectedTeamIndex: 0
    });

    componentDidMount() {
        this.unsubsribe = this.setState({loadingModalOpen: true}, () => {
            db.collection("games")
                .doc(this.props.match.params.id)
                .onSnapshot((snapshot) => {
                    if (snapshot.data()) {
                        this.setState({game: snapshot.data(), loadingModalOpen: false});
                    }
                }, () => this.setState({loadingModalOpen: false, game: null}));
        });
    }

    componentWillUnmount() {
        this.unsubsribe && this.unsubsribe();
    }

    render() {
        if(this.state.loadingModalOpen) {
            return <LoadingModal isVisible={true} text={"Loading game..."}/>
        }
        if(!this.state.game) {
          return (
              <div>
                  <h3>No game found with the ID {this.props.match.params.id}</h3>
              </div>
          );
        }
        const teams = this.state.game.teams;
        const groupIsClosed = key => this.behaviour.groupIsClosed(teams, key);
        const pointsAvailable = (scoreKey, teamIndex) => this.behaviour.pointsAvailable(teams, scoreKey, teamIndex);
        return (
            <div>
                <ScoreModal
                    isOpen={this.state.scoreModalOpen}
                    multiplier={this.state.selectedTeamScoreMultiplier}
                    addByMultiplier={this.addByMultiplier}
                    onCancel={this.onScoreModalCancel}/>
                <ScoreboardPanels
                    teams={teams}
                    groupIsClosed={groupIsClosed}
                    pointsAvailable={pointsAvailable}
                    onSubmitTeam={e => this.onSubmitTeam(e)}
                    onUpdateTeam={e => this.onUpdateTeam(e)}
                    onSubmitPoints={e => this.onSubmitPoints(e)}
                    onUpdatePoints={e => this.onUpdatePoints(e)}
                    decreaseScore={(teamIndex, scoreKey) => this.decreaseScore(teamIndex, scoreKey)}
                    increaseScore={(teamIndex, scoreKey) => this.increaseScore(teamIndex, scoreKey)}
                    addTeam={e => this.addTeam(e)}
                    />
            </div>
        );
        
    }
}

export default Scoreboard;

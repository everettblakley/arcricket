import React, {Component} from "react";

class Leaderboard extends Component {
	render() {
		return (
			<div className={"landing"}>
				<h1>This is the leaderboard</h1>
				<h5>Once we play some more games, stats will show up here</h5>
				<h5>For now, here's some dummy data</h5>
			</div>
		);
	}
}

export default Leaderboard;
